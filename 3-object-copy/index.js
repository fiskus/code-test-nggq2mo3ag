'use strict';

var assert = require('assert');
var isEqual = require('lodash.isequal');

function copy(input) {
    if (Array.isArray(input)) {
        return copyArray(input);
    }
    if (typeof input === 'object' && input !== null) {
        return copyObject(input);
    }
    return input;
}

function copyArray(input) {
    return input.map(copy);
}

function copyObject(input) {
    let output = {};
    let keys = Object.keys(input);
    keys.forEach(key => {
        output[key] = copy(input[key]);
    });
    return output;
}


var strIn = 'string';
var strOut = copy(strIn);

var numIn = 123;
var numOut = copy(numIn);

var boolIn = true;
var boolOut = copy(boolIn);

var nullIn = null;
var nullOut = copy(nullIn);

var nanIn = NaN;
var nanOut = copy(nanIn);

var simpleArrIn = [1, 2, 3];
var simpleArrOut = copy(simpleArrIn);

var complexArrIn = [1, 2, 3, [4, 5, 6, [7, 8]], 9, 10];
var complexArrOut = copy(complexArrIn);

var simpleObjIn = {
    key1: 'string',
    key2: 123,
    key3: true
};
var simpleObjOut = copy(simpleObjIn);

var complexObjIn = {
    key1: {
        key1: 'string',
        key2: 123,
        key3: simpleObjIn
    },
    key2: [1, 2, 3, [1, 2, 3]]
};
var complexObjOut = copy(complexObjIn);


function testPrimitive(a, b) {
    assert.equal(a, b);
    assert(isEqual(a, b), true);
}

function testNaN(a, b) {
    assert.notEqual(a, b);
    assert(isEqual(a, b), true);
}

function testObject(a, b) {
    assert.notEqual(a, b);
    for (var key in a) {
        if (typeof a[key] === 'object') {
            assert.notEqual(a[key], b[key]);
        }
    }
    assert.equal(isEqual(a, b), true);
}

testPrimitive(strIn, strOut);
testPrimitive(numIn, numOut);
testPrimitive(boolIn, boolOut);
testPrimitive(nullIn, nullOut);

testNaN(nanIn, nanOut);

testObject(simpleArrIn, simpleArrOut);
testObject(complexArrIn, complexArrOut);

testObject(simpleObjIn, simpleObjOut);
testObject(complexObjIn, complexObjOut);
