'use strict';

/* global $ */

var spinner;
var chartList;
var gameIndex = 0;
var gamesCache = {};

function loadChart() {
    spinner.show();

    var CHART_URL = '/thisweek.json';
    $.ajax(CHART_URL)
        .then(onChart)
        .then(loadGames)
        .done(finish)
        .fail(function(err) {
            throw Error(err);
        });
}

function onChart(chart) {
    setTitle(chart.week);
    return chart.games;
}

function setTitle(title) {
    $('.chart-title').text(title);
}

function loadGames(games) {
    gamesCache = games;
    if (games[gameIndex]) {
        return loadGame(games[gameIndex].url);
    } else {
        return null;
    }
}

function loadGame(url) {
    return $.ajax(url)
        .then(onGame);
}

function onGame(game) {
    appendScores(game.teams);
    gameIndex++;
    return loadGames(gamesCache);
}

function appendScores(teams) {
    var text = [
        teams[0].name,
        '<span class="chart-score">' + teams[0].score + '</span>',
        '—',
        '<span class="chart-score">' + teams[1].score + '</span>',
        teams[1].name
    ];
    var chartItem = $('<li class="chart-item">' + text.join(' ') + '</li>');
    chartList.append(chartItem);
}

function fulfillElements() {
    chartList = $('.chart-list');
    spinner = $('.spinner');
}

function finish() {
    gameIndex = 0;
    gamesCache = {};
    spinner.hide();
}

$(function() {
    fulfillElements();
    loadChart();
});
