'use strict';

var tabDemo = function() {
    for (var i = 0; i < document.body.children.length; i++) {
        var child = document.body.children[i];
        if (child.tagName.toLowerCase() !== 'section') {
            continue;
        }

        var t = new TabPanel();
        t.init(child);
        window.panels.push(t);
    }
};
var panels = [];
window.onload = tabDemo;

/* ---------------------------------------------- */

var TabPanel = function() {
    this._panelIndex = panels.length;
    this._tabIndex = 0;
    this._panels = window.panels;
};

TabPanel.prototype.init = function(section) {
    this._panel = section;
    this._panel.addEventListener('click', this.onSectionClick.bind(this));

    this._tabs = [].slice.call(this._panel.querySelectorAll('article'));

    this.setPanelActive();
};

TabPanel.prototype.onSectionClick = function(event) {
    this.setPanelActive();

    if (TabPanel.isTabHeader(event.target)) {
        var newIndex = this.getIndexFromTab(event.target.parentNode);
        this.setTabActive(newIndex);
    }
};

TabPanel.prototype.getIndexFromTab = function(inputTab) {
    var newIndex = 0;
    this._tabs.some(function(tab, index) {
        newIndex = index;
        return tab === inputTab;
    }, this);
    return newIndex;
};

TabPanel.prototype.setPanelActive = function() {
    this._panel.classList.add('active');
    this.setOtherPanelsInactive();
    this.setTabActive(0);
};

TabPanel.prototype.setPanelInactive = function() {
    this._panel.classList.remove('active');
};

TabPanel.prototype.setOtherPanelsInactive = function() {
    this._panels.forEach(function(panel, index) {
        if (index !== this._panelIndex) {
            panel.setPanelInactive();
        }
    }, this);
};

TabPanel.prototype.setTabActive = function(newIndex) {
    if (newIndex !== this._tabIndex) {
        this._tabs[this._tabIndex].classList.remove('active');
    }
    this._tabs[newIndex].classList.add('active');
    this._tabIndex = newIndex;
};

TabPanel.isTabHeader = function(element) {
    return element.tagName.toLowerCase() === 'h3';
};
