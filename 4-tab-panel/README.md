4. Given the following `tab-panel.html`:

  ```html
  <html>
    <head>
      <link href="tab-panel.css" rel='stylesheet' type='text/css'></link>
    </head>
    <body>
    	<section>
    		<h2>Tab Panel One</h2>
    		<article title="Tab One">
    			<h3>Tab One</h3>
    			<p>Panel One - tab One</p>
    		</article>

    		<article title="Tab Two">
    			<h3>Tab Two</h3>
    			<p>Panel One - tab Two</p>
    		</article>
    	</section>
    	<section>
    		<h2>Tab Panel Two</h2>
    		<article title="Tab One">
    			<h3>Tab One</h3>
    			<p>Panel Two - Tab One</p>
    		</article>

    		<article title="Tab Two">
    			<h3>Tab Two</h3>
    			<p>Panel Two - tab Two</p>
    		</article>
    	</section>
    	<script src="tab-panel.js" type='text/javascript'></script>
    </body>
  </html>
  ```

  And the following `tab-panel.js` part:

  ```js
  var tabDemo = function() {
  	for(var i = 0; i < document.body.children.length; i++) {
  		child = document.body.children[i];
  		if(child.tagName.toLowerCase() != 'section') continue;

  		t = new TabPanel();
  		t.init(child);
  		window.panels.push(t);
  	}
  };
  var panels = [];
  window.onload = tabDemo;
  ```

  Without using jQuery, implement the `TabPanel` component that handles tabs switching. Add any applicable stylings to `tab-panel.css` (we aren't assessing artistry abilities - we are just after clean functional css).
