Как проверить задания:

    $ git clone git@git.chervonny.ru/open/coding-test
    $ npm install

Я использую node 4, в коде есть ES6-специфичный синтаксис.

## 1

[Код решения](1-partial-application/index.js)

Проверка правильности осуществляется assert'ами (саму задачу я слегка усложнил).

    $ cd 1-partial-application
    $ node index.js

## 2

[Код решения](2-json/public/script.js)

Сервер на express. Названия команд и очки [создаются на лету](2-json/routes/index.js).

Для простоты загрузка последовательная, а не очередь.

    $ cd 2-json
    $ ./bin/www

Дальше открыть в браузере http://localhost:3000

## 3 

[Код решения](3-object-copy/index.js)

Опять assert'ы. Проверка правильности копирования осуществляется _.isEqual

    $ cd 3-object-copy
    $ node index.js

## 4

[Код решения](4-tab-panel/tab-panel.js)

Просто в браузере открыть 4-tab-panel/index.html
