2. Say you have a `http://basketball.example.com/thisweek.json` HTTP endpoint that returns the following JSON:

  ```json
  {
    "week": "51st of 2011",
    "games": [
      { "url": "http://basketball.example.com/game/1" },
      { "url": "http://basketball.example.com/game/2" },
      { "url": "http://basketball.example.com/game/3" },
      { "url": "http://basketball.example.com/game/4" }
    ]
  }
  ```

  The games entry is dynamic and may include arbitrary URLÃ¢â‚¬â„¢s.
  Each of the URL endpoints returns some game data:

  ```json
  {
    "id": "1",
    "teams": [
      {
        "name": "Lakers",
        "score": 79
      },
      {
        "name": "Bulls",
        "score": 99
      }
    ]
  }
  ```

  Given that jQuery is available and loaded, write a summary function that prints a list of all scores formatted like `Lakers 79 - 99 Bulls`.
