'use strict';

var express = require('express');

var router = express.Router();

function renderJson(res, json) {
    res.set('Content-Type', 'application/json; charset=utf-8');
    res.send(json);
}

function genGames(base) {
    var games = [];
    let i;
    for (i = 0; i < 40; i++) {
        games.push({
            url: `${base}/game/${i}`
        });
    }
    return games;
}

let alphabet = 'abcdef ghigkl mnopqr stuvwxyz ';

function genLetter() {
    let rand = parseInt(Math.random() * 29, 10);
    return alphabet[rand];
}

function genWordLength() {
    let length = parseInt(Math.random() * 20, 10);
    return length < 5 ? 5 : length;
}

function genWord() {
    let wordLength = genWordLength();
    let word = [genLetter().toUpperCase()];
    let i;
    for (i = 1; i < wordLength; i++) {
        word.push(genLetter());
    }
    return word.join('').trim();
}


router.get('/thisweek.json', function(req, res) {
    renderJson(res, {
        week: '51st of 2011',
        games: genGames(req.protocol + '://' + req.get('host'))
    });
});

router.get('/game/:num', function(req, res) {
    renderJson(res, {
        id: req.params.num.toString(),
        teams: [{
            name: genWord(),
            score: parseInt(Math.random() * 100, 10)
        }, {
            name: genWord(),
            score: parseInt(Math.random() * 100, 10)
        }]
    });
});

router.get('/', function(req, res) {
    res.render('index');
});

module.exports = router;
