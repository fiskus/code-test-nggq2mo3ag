'use strict';

var assert = require('assert');

Object.defineProperty(Function.prototype, 'magic', {
    value: magic
});

function magic() {
    let that = this;
    let preArgs = [].slice.call(arguments);
    return function() {
        let postArgs = [].slice.call(arguments);
        let args = [].concat.apply([], preArgs);
        args = [].concat.apply(args, postArgs);
        return that.apply(that, args);
    };
}

function add(a, b) {
    return a + b;
}

function say(something) {
    return something;
}

function sayMore(a, b, c) {
    return a + b + c;
}

var addTo = add.magic(2);
var welcome = say.magic('Hi, how are you?');
var hello = sayMore.magic('He', 'll');

assert.equal(addTo(5), 7);
assert.equal(welcome(), 'Hi, how are you?');
assert.equal(hello('o!'), 'Hello!');
