1. Without using Javascript's `bind` function, implement the `magic` function so that:

  ```js
  var add = function(a, b) { return a + b; }
  var addTo = add.magic(2);

  var say = function(something) { return something; }
  var welcome = say.magic('Hi, how are you?');

  addTo(5) == 7;
  welcome() == 'Hi, how are you?';
  ```
